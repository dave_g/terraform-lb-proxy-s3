variable vpc_id {}
variable service_name {}
variable subnets {
  type = "list"
}
variable private_dns_enable {}
variable security_group_id {}


resource "aws_vpc_endpoint" "vpc_endpoint" {
  vpc_id            = "${var.vpc_id}"
  service_name      = "${var.service_name}"
  vpc_endpoint_type = "Interface"
  subnet_ids = "${var.subnets}"
  private_dns_enabled = "${var.private_dns_enable}"
  security_group_ids = [
    "${var.security_group_id}",
  ]
}
