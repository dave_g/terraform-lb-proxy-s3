//rescource "type" "name_to_use"
resource "aws_instance" "example" {
  ami           = "ami-099e057020b344244"
  instance_type = "t2.micro"
}

resource "aws_eip" "ip" {
  instance = "${aws_instance.example.id}" //interpolation to definded resource
}
