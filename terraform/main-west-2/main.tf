locals {
  //orgs often delegate network config to an authorized group so this script assumes the vpc is already built and network is configured. 
  region                = "us-west-2" //set your region
  vpc_id                = "" // add your vpc is
  cidr                  = "10.146.0.0/16" // add the configured CIDR 
  // blue-green deployment. two subnets/AZs will be used. LB will use both but only one is active at a given time.
  subnet1               = ""   // add subnet id ex. subnet-0aefe9adevc8d99
  subnet2               = ""   //add subnet id 2
  
  auto_scaling_subnets  = ["subnet1"]  //set this to bounce instances between subnets/az.
  load_balancer_subnets = ["subnet1", "subnet2"]
  route_tables          = [""] //route table ex. rtb-012f4509c5c6f2 

  //services
  service_name     = "com.amazonaws.us-west-2.s3" //s3 service endpoint name. change to correct region
  sqs_service_name = "com.amazonaws.us-west-2.sqs" //sqs service

  //EC2
  key_name         = "ec2_key" // ec2 ssh key name 
  auto_scale_type  = "t2.micro"              //"t2.micro", m5.large ect.
  auto_scale_ami   = "ami-123456adfc"
  proxy_ec2_role  = "ec2-instance-s3-logging" //instance role for accessing s3 etc..
  //load balancer
  delete_protect   = "true" // protect the load balancer from destroy. loses it's statc ip otherwise. eliviate this issue by integrating w/ route53. 

  //security group cidrs
  cidr_blocks    = ["0.0.0.0/0"]                                                           
  s3_cidr_blocks = ["52.92.48.0/22", "52.219.20.0/22", "52.219.24.0/21", "54.231.232.0/21"] // s3 cidres

  // todo : re-evaluate tag passing after 0.12 terraform is released that will Support count in resource fields #7034 - https://github.com/hashicorp/terraform/issues/7034
  tag_app_name = "S3-VPC"
  tag_env      = "production"
  tag_lb_name          = "Proxy-LB"
  tag_ec2_name         = "S3-Proxy-Server"
}

provider "aws" {
  region                  = local.region
  profile                 = "" //security profile
  allowed_account_ids     = ["123456789"] // restrict to your aws account #
  shared_credentials_file = "~/.aws/credentials"
  version                 = "~> 2.19.0"
}

module "acls" {
  source  = "../acls"
  subnet1 = local.subnet1
  subnet2 = local.subnet2
  vpc_id  = local.vpc_id
}

//local cidr block all protocol rules
module "acl_cidr_rules" {
  source           = "../acl_rules"
  rule_number_seed = 100
  cidr_blocks      = local.cidr_blocks
  network_acl_id   = module.acls.acl_id
}

//s3 cidr block rules
module "acl_s3_rules" {
  source           = "../acl_rules"
  cidr_blocks      = local.s3_cidr_blocks
  network_acl_id   = module.acls.acl_id
  rule_number_seed = 200
  protocol         = "6"
  from_port        = "1024"
  to_port          = "65535"
}

//all outbound
module "acl_outbound_rules" {
  source           = "../acl_rules"
  cidr_blocks      = ["0.0.0.0/0"]
  network_acl_id   = module.acls.acl_id
  rule_number_seed = 2000
  protocol         = "-1"
  egress           = "true"
}

module "sgs" {
  source              = "../securityGroups"
  vpc_id              = local.vpc_id
  ingress_cidr_blocks = local.cidr_blocks
  egress_cidr_blocks  = ["0.0.0.0/0"]

  tag_app_name = local.tag_app_name
  tag_env      = local.tag_env
  tag_name             = "local-access"
  name                 = "local-access"
  description          = "allows local addresses access to the vpc."
}

module "loadBalancer" {
  source = "../loadBalancer"
  load_balancer_subnets = local.load_balancer_subnets
  auto_scaling_subnets  = local.auto_scaling_subnets
  delete_protect        = local.delete_protect
  vpc_id                = local.vpc_id
  auto_scale_ami        = local.auto_scale_ami
  auto_scale_type       = local.auto_scale_type
  security_group        = module.sgs.id //get the security group id created in sgs module
  key_name              = local.key_name
  ec2_role              = local.proxy_ec2_role

  ec2_tags = list(
  map("key", "ApplicationName", "value", local.tag_app_name, "propagate_at_launch", true),
  map("key", "Name", "value", local.tag_ec2_name, "propagate_at_launch", true),
  map("key", "ApplicationEnvironment", "value", local.tag_env, "propagate_at_launch", true)
  )

  tag_app_name = local.tag_app_name
  tag_env      = local.tag_env
  tag_name      = local.tag_lb_name
}

module "endPoint" {
  source       = "../endpoint"
  vpc_id       = local.vpc_id
  route_tables = local.route_tables
  service_name = local.service_name
}

module "interfaceEndPoint" {
  source             = "../interfaceEndPoint"
  vpc_id             = local.vpc_id
  subnets            = local.load_balancer_subnets
  service_name       = local.sqs_service_name
  private_dns_enable = true
  security_group_id  = module.sgs.id //get the security group id created in sgs module
}

output "load-balancer-id" {
  value = module.loadBalancer.load-balancer-id
}

output "load-balancer-dns" {
  value = module.loadBalancer.load-balancer-dns
}
