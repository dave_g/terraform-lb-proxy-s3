variable protocol {
  default = "-1"
}
variable from_port {
  default = "0"
}
variable to_port {
  default = "0"
}

variable egress {
  default = false
}
variable "rule_action" {
  default = "allow"
}
variable "cidr_blocks" {
  type = "list"
}
variable rule_number_seed {
  default = 100
}

variable "network_acl_id" {
  default = ""
}

locals {
  cidr_count = "${length(var.cidr_blocks)}"
}

resource "aws_network_acl_rule" "all-local-networks" {
  count = "${local.cidr_count}"
  network_acl_id = "${var.network_acl_id}"
  rule_number    = "${var.rule_number_seed + count.index}"
  egress         = "${var.egress}"
  protocol       = "${var.protocol}"
  rule_action    = "${var.rule_action}"
  cidr_block     = "${var.cidr_blocks[count.index]}"
  from_port      = "${var.from_port}"
  to_port        = "${var.to_port}"
}
