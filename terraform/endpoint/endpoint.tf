variable vpc_id {}
variable service_name {}
variable route_tables {
  type = "list"
}

resource "aws_vpc_endpoint" "s3_service_endpoint" {
  vpc_id            = "${var.vpc_id}"
  service_name      = "${var.service_name}"
  vpc_endpoint_type = "Gateway"
  route_table_ids = "${var.route_tables}"
}
