variable conditional_create {
  default = true
}

variable cidr {}
variable tag_name {}


resource "aws_vpc" "main" {
  count = "${var.conditional_create ? 1 : 0}"
  cidr_block = "${var.cidr}"

  tags {
    Name = "${var.tag_name}"
  }
}

output "vpc-id" {
  // https://github.com/hashicorp/terraform/issues/16726
  //https://www.terraform.io/upgrade-guides/0-11.html#referencing-attributes-from-resources-with-count-0
  value = "${element( concat(aws_vpc.main.*.id, list("") ), 0)}"
}
