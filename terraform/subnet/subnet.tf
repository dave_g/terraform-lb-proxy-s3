variable conditional_create {
  default = true
}

variable cidr {}
variable tag_name {}
variable vpc_id {}

resource "aws_subnet" "main" {
  count = "${var.conditional_create ? 1 : 0}"
  vpc_id     = "${var.vpc_id}"
  cidr_block = "${var.cidr}"

  tags {
    Name =  "${var.tag_name}"
  }
}

output "subnet-id" {
  // https://github.com/hashicorp/terraform/issues/16726
  //https://www.terraform.io/upgrade-guides/0-11.html#referencing-attributes-from-resources-with-count-0
  value = "${element( concat(aws_subnet.main.*.id, list("") ), 0)}"  
}
