variable conditional_create {
  default = true
}

variable tag_name {}
variable vpc_id {}

resource "aws_route_table" "main" {
  count = "${var.conditional_create ? 1 : 0}"
  vpc_id =  "${var.vpc_id}"

  tags {
    Name =  "${var.tag_name}"
  }
}

output "route-table-id" {
  // https://github.com/hashicorp/terraform/issues/16726
  //https://www.terraform.io/upgrade-guides/0-11.html#referencing-attributes-from-resources-with-count-0
  value = "${element( concat(aws_route_table.main.*.id, list("") ), 0)}"
}
