//variable subnet1 {}
//variable subnet2 {}
variable load_balancer_subnets {
  type = "list"
}
variable auto_scaling_subnets {
  type = "list"
}
variable delete_protect {}
variable vpc_id {}
variable auto_scale_ami {}
variable auto_scale_type {}
variable security_group {}
variable key_name {}
variable ec2_tags {
  type = "list"
}
variable tag_app_name {}
variable tag_env {}
variable tag_name {}
variable "ec2_role" {}

resource "aws_lb" "load-balancer" {
  name               = "proxy-network-lb"
  internal           = true
  load_balancer_type = "network"
  subnets            = "${var.load_balancer_subnets}"//["${var.subnet1}", "${var.subnet2}"]

  enable_deletion_protection = "${var.delete_protect}"

  tags = {
    "ApplicationName" = "${var.tag_app_name}"
    "ApplicationEnvironment" = "${var.tag_env}"
    "Name" = "${var.tag_name}"
  }
}

resource "aws_lb_target_group" "target-group" {
  name     = "proxy-lb-target-group"
  port     = 3128
  protocol = "TCP"
  vpc_id   = "${var.vpc_id}"
  depends_on = ["aws_lb.load-balancer"]
}

resource "aws_lb_listener" "lb-listener-443" {
  load_balancer_arn = "${aws_lb.load-balancer.arn}"
  port              = "443"
  protocol          = "TCP"

  default_action {
    target_group_arn = "${aws_lb_target_group.target-group.arn}"
    type             = "forward"
  }
}

resource "aws_lb_listener" "lb-listener-80" {
  load_balancer_arn = "${aws_lb.load-balancer.arn}"
  port              = "80"
  protocol          = "TCP"

  default_action {
    target_group_arn = "${aws_lb_target_group.target-group.arn}"
    type             = "forward"
  }
}

resource "aws_lb_listener" "lb-listener-3128" {
  load_balancer_arn = "${aws_lb.load-balancer.arn}"
  port              = "3128"
  protocol          = "TCP"

  default_action {
    target_group_arn = "${aws_lb_target_group.target-group.arn}"
    type             = "forward"
  }
}


// auto scaling stuff
resource "aws_launch_configuration" "launch-configuration" {
  name_prefix   = "proxy-server-launch-config"
  image_id      = "${var.auto_scale_ami}"
  instance_type = "${var.auto_scale_type}"
  security_groups = ["${var.security_group}"]
  iam_instance_profile = "${var.ec2_role}"
  key_name = "${var.key_name}"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "auto-scale-group" {
  vpc_zone_identifier = "${var.auto_scaling_subnets}" //["${var.subnet1}", "${var.subnet2}"]
  desired_capacity = 1
  max_size = 4
  min_size = 1
  target_group_arns = ["${aws_lb_target_group.target-group.arn}"]
  launch_configuration = "${aws_launch_configuration.launch-configuration.name}"

  //tags = "${var.ec2_tags}"
  dynamic "tag" {
    for_each = var.ec2_tags
    content {
      key = tag.value.key
      value = tag.value.value
      propagate_at_launch = tag.value.propagate_at_launch
    }
  }
}

resource "aws_autoscaling_policy" "autoscale-policy" {
  name = "${var.tag_name}-autoscaling-policy"
  autoscaling_group_name = "${aws_autoscaling_group.auto-scale-group.name}"
  policy_type = "TargetTrackingScaling" // "SimpleScaling", "StepScaling" or "TargetTrackingScaling"
  estimated_instance_warmup = 420 //time in seconds -defaults to cooldown
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageNetworkIn"
    }
    target_value = 900000000
    disable_scale_in = false //(Optional, Default: false) Indicates whether scale in by the target tracking policy is disabled.
  }
  //adjustment_type        = "ChangeInCapacity" //(Optional) Specifies whether the adjustment is an absolute number or a percentage of the current capacity. Valid values are ChangeInCapacity, ExactCapacity, and PercentChangeInCapacity.
  //cooldown               = 300 //only available to "SimpleScaling"
  //scaling_adjustment     = 4 //only available to "SimpleScaling"
}



output "load-balancer-id" {
  value = "${aws_lb.load-balancer.id}"
}

output "load-balancer-dns" {
  value = "${aws_lb.load-balancer.dns_name}"
}
