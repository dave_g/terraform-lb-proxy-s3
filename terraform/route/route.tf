variable route_table_id {}
variable cidr {}
variable tag_name {}


resource "aws_route" "route" {
  route_table_id            = "${var.route_table_id}"
  destination_cidr_block    = "${var.cidr}"
}

output "route-id" {
  value = "${aws_route.route.id}"
}
